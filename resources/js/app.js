require('./bootstrap');

import Echo from "laravel-echo"

window.io = require('socket.io-client');

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});

// if (typeof io !== 'undefined') {
//     window.Echo = new Echo({
//         broadcaster: 'socket.io',
//         host: window.location.hostname + ':6001',
//     });
// }

let onlineUsers = 0;

window.Echo.join(`online`)
    .here((users) => {
        let userId = $('meta[name=user-id]').attr('content');
        onlineUsers = users.length;
        if (onlineUsers > 1) {
            $('#no-online-users').css('display', 'none');
        }

        users.forEach(user => {
            if (user.id != userId) {
                $('#online-users').append(`<li id="user-${user.id}" class="list-group-item"><i class="fas fa-circle text-success"></i>${user.name} </li>`);
            }
        }); //end foreach
        // console.log(users)
    })
    .joining((user) => {


        $('#no-online-users').css('display', 'none');

        $('#online-users').append(`<li id="user-${user.id}" class="list-group-item">    <i class="fas fa-circle text-success"></i>        ${user.name} </li>`);
        onlineUsers++;
        // console.log(user.name);
    })
    .leaving((user) => {

        $("#user-" + user.id).remove();
        onlineUsers--;

        if (onlineUsers == 1) {
            $('#no-online-users').css('display', 'block');
        }
        // console.log(user.name);
    }).listen('MessageDelivered', (e) => {
        // console.log(e.message.body);
        console.log('chat');
    });


$('#chat-text').keypress(function(e) {
    // console.log(e.which);
    if (e.which == 13) {
        e.preventDefault();
        let body = $(this).val();
        let url = $(this).data('url');
        let data = {
            '_token': $('meta[name=csrf-token]').attr('content'),
            body
        };

        $.ajax({
            url: url,
            method: 'post',
            data: data,
        });
    }


});

window.Echo.channel('chat-group')
    .listen('MessageDelivered', (e) => {
        console.log(e.message.body);
    });