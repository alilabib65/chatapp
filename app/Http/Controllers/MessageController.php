<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;
use App\Events\MessageDelivered;

class MessageController extends Controller
{
    
    public function index()
    {
        $messages = Message::all();
        return view('messages.index',compact('messages'));
    }//end of index  


    public function store(Request $request)
    {
        $message = auth()->user()->messages()->create($request->all());
        broadcast(new MessageDelivered($message))->toOthers();
    }

}//end of controller
